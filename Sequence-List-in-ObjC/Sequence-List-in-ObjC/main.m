//
//  main.m
//  Sequence-List-in-ObjC
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SequenceList.h"

void testSequenceList();

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"testSequenceList");
        testSequenceList();
    }
    return 0;
}

void testSequenceList() {
    SequenceList *sqList = [[SequenceList alloc] initWithListSize:5];
    
    [sqList insertAt:0 Elem:1];
    [sqList insertAt:1 Elem:2];
    [sqList insertAt:2 Elem:3];
    [sqList insertAt:3 Elem:4];
    [sqList insertAt:4 Elem:5];
    [sqList insertAt:5 Elem:6];
    [sqList insertAt:6 Elem:7];
    [sqList insertAt:-1 Elem:-1];
    
    [sqList listTraverse];
    
    int e = 0;
    if ([sqList getAt:6 Elem:&e]) {
        NSLog(@"e: %d", e);
    }
    
    if ([sqList getAt:4 Elem:&e]) {
        NSLog(@"e: %d", e);
    }
    
    if ([sqList deleteAt:1 Elem:&e]) {
        NSLog(@"delete: %d", e);
    }
    
    if ([sqList deleteAt:-1 Elem:&e]) {
        NSLog(@"delete: %d", e);
    }
    
    [sqList listTraverse];
    
    if ([sqList priorElem:2 Pre:&e]) {
        NSLog(@"%d, 2", e);
    }
    
    if ([sqList nextElem:3 Next:&e]) {
        NSLog(@"3, %d", e);
    }
    
    [sqList clearList];
    
    [sqList listTraverse];
}
