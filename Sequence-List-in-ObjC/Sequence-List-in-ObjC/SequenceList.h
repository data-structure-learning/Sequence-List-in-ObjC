//
//  SequenceList.h
//  Sequence-List-in-ObjC
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SequenceList : NSObject {
    @private
    int *m_pList;
    int m_iSize;
    int m_iLen;
}

- (instancetype)initWithListSize:(int)size;
- (void)clearList;
- (BOOL)isEmpty;
- (BOOL)isFull;
- (int)listLen;
- (BOOL)getAt:(int)i Elem:(int *)e;
- (int)locateElem:(int)e;
- (BOOL)priorElem:(int)curElem Pre:(int *)preElem;
- (BOOL)nextElem:(int)curElem Next:(int *)nextElem;
- (BOOL)insertAt:(int)i Elem:(int)elem;
- (BOOL)deleteAt:(int)i Elem:(int *)elem;
- (void)listTraverse;

@end
