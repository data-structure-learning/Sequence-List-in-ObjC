//
//  SequenceList.m
//  Sequence-List-in-ObjC
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import "SequenceList.h"

@implementation SequenceList

- (instancetype)initWithListSize:(int)size {
    self = [super init];
    if (self) {
        m_iSize = size;
        m_pList = (int *)malloc(sizeof(int)*m_iSize);
        [self clearList];
    }
    return self;
}

- (void)clearList {
    m_iLen = 0;
}

- (BOOL)isEmpty {
    return m_iLen == 0;
}

- (BOOL)isFull {
    return m_iLen == m_iSize;
}

- (int)listLen {
    return m_iLen;
}

- (BOOL)getAt:(int)i Elem:(int *)e {
    if (i < 0 || i >= m_iLen) {
        return NO;
    }
    
    *e = m_pList[i];
    return YES;
}

- (int)locateElem:(int)e {
    for (int i = 0; i < m_iLen; i ++) {
        if (e == m_pList[i]) {
            return i;
        }
    }
    
    return -1;
}

- (BOOL)priorElem:(int)curElem Pre:(int *)preElem {
    int t = [self locateElem:curElem];
    if (t == 0 || t == -1) {
        return NO;
    }
    
    *preElem = m_pList[t - 1];
    return YES;
}

- (BOOL)nextElem:(int)curElem Next:(int *)nextElem {
    int t = [self locateElem:curElem];
    if (t == m_iLen - 1 || t == -1) {
        return NO;
    }
    
    *nextElem = m_pList[t + 1];
    return YES;
}

- (BOOL)insertAt:(int)i Elem:(int)elem {
    if (i < 0 || i > m_iLen || [self isFull]) {
        return NO;
    }
    
    for (int j = m_iLen - 1; j > i; j --) {
        m_pList[j + 1] = m_pList[j];
    }
    
    m_pList[i] = elem;
    m_iLen += 1;
    return YES;
}

- (BOOL)deleteAt:(int)i Elem:(int *)elem {
    if (i < 0 || i >= m_iLen || [self isEmpty]) {
        return NO;
    }
    
    *elem = m_pList[i];
    
    for (int j = i; j < m_iLen - 1; j ++) {
        m_pList[j] = m_pList[j + 1];
    }
    m_iLen -= 1;
    return YES;
}

- (void)listTraverse {
    for (int i = 0; i < m_iLen; i ++) {
        NSLog(@"%d", m_pList[i]);
    }
    NSLog(@"\n");
}

@end
